FROM ubuntu:16.04

RUN apt-get update \
 && apt-get dist-upgrade -y \
 && apt-get install -y \
     live-build \
     syslinux-themes-ubuntu-xenial \
     gfxboot-theme-ubuntu \
     livecd-rootfs \
     syslinux \
     librsvg2-bin \
     isolinux \
     syslinux-utils \
     xorriso \
 && mkdir -p /srv/image-builder/

WORKDIR /srv/image-builder/

CMD /srv/image-builder/build.sh
