#!/bin/sh

# Doc and examples
# http://debian-live.alioth.debian.org/live-manual/stable/manual/html/live-manual.en.html#396
# https://wiki.kubuntu.org/Live-Build
# https://github.com/simonpoole1/raspbian-live-build
# https://docs.kali.org/kali-dojo/02-mastering-live-build
# https://lists.debian.org/debian-live/2011/06/msg00152.html
# https://github.com/vyos/build-iso
# https://github.com/xbmc/XBMCbuntu/tree/master/configFiles/includes.chroot/etc


auto/clean

export SUITE=artful
export ARCH=i386
export LB_ARCHITECTURES=i386
export PROJECT=ubuntu
export BINARYFORMAT=iso-hybrid
export LB_SYSLINUX_THEME=ubuntu-xenial
export LB_HDD_LABEL="AlternOS"
export LB_ISO_APPLICATION="AlternOS Live"
export LB_ISO_VOLUME="AlternOS $(date +%Y%m%d-%H:%M)"
auto/config

# Copy packages to install on the system
cp desktop.list.chroot config/package-lists/

# Copy keys and PPA / repositories sources.
cp archives/* config/archives/

# Copy our hooks
cp local/hooks/* config/hooks/

auto/build --debug
